#include <iostream>

class Element{
public:
    int data;
    Element* parent;
    Element* right;
    Element* left;
    Element(int d){
        data=d;
        right=NULL;
        left=NULL;
        parent=NULL;
    }
};

Element* root;

class Tree{
public:
    void insert (int d, Element* node);
    void in_order_tree_walk (Element* node);
    Element* find(Element *node, int value);
    Element* min(Element *node);
    void remove(Element *node);

    Tree () {
        root=NULL;
    }
};

void Tree::in_order_tree_walk (Element* node)
{
    if(node->left != NULL) //jezeli ma dzieci po lewej stronie wywolaj funkcje rekurencyjnie
        in_order_tree_walk(node->left);

    std::cout<<node->data<<std::endl; //wypisz wartosc

    if(node->right != NULL) //jezeli ma dzieci po prawej stronie wywolaj rekurencyjnie
        in_order_tree_walk(node->right);
}

void Tree::insert(int d, Element *node) {
    if (root == NULL){
        root = new Element(d);
    }
    else if (d < node->data){
        if (node->left != NULL){
            insert(d, node->left);
        }
        else {
            Element* temp = new Element (d);
            temp->parent = node;
            node->left = temp;
        }
    }
    else {
        if (node->right != NULL){
            insert(d, node->right);
        }
        else {
            Element* temp = new Element (d);
            temp->parent = node;
            node->right = temp;
        }
    }
}

Element* Tree::find(Element *node, int value)
{
//jezeli wezel ma szukana wartosc to odnalezlismy go
    if (node->data == value) return node;
//jezeli szukana wartosc jest mniejsza to szukamy w lewym poddrzewie o ile istnieje
    else if (value < node->data && node->left != NULL) return find(node->left, value);
//jezeli szukana wartosc jest wieksza to szukamy w prawym poddrzewie o ile istnieje
    else if (value > node->data && node->right != NULL) return find(node->right, value);
    return NULL;
}

Element* Tree::min(Element *node)
{
    if(node->left != NULL)
        return min(node->left);
    else
        return node;
}

void Tree::remove(Element *node)
{

/*I.wezel nie ma dzieci*/if(node->left == NULL && node->right == NULL)
    {

        if(node->parent == NULL) //wezel korzeniem
        {
            root=NULL;
        }
        else if(node->parent->left == node) //wezel po lewej stronie
        {
            node->parent->left = NULL;      //usun ten wezel
        }
        else                                //wezel po prawej stronie
        {
            node->parent->right = NULL;     //usun ten wezel
        }
        delete node;
    }

/*II. wezel ma jedno dziecko*/else if(node->left==NULL || node->right==NULL)
    {
/*1. po lewej stronie wezla nie ma dziecka*/if(node->left==NULL)
        {
/*a*/            if(node->parent==NULL)             //wezel korzeniem
            {
                root=node->right;
            }
/*b*/            else if(node->parent->left==node)  //lewe poddrzewo
            {
                node->parent->left=node->right;     //na miejsce wezla prawe dziecko
            }
/*c*/            else                               //prawe poddrzewo
            {
                node->parent->right=node->right;    //na miejsce wezla prawe dziecko
            }
        }
/*2. po prawej stronie wezla nie ma dziecka*/else
        {
            if(node->parent==NULL)                  //wezel korzeniem
            {
                root=node->left;
            }
            else if(node->parent->left==node)      //lewe poddrzewo
            {
                node->parent->left=node->left;    //na miejsce wezla lewe dziecko
            }
            else                                    //prawe poddrzewo
            {
                node->parent->right=node->left;    //na miejsce wezla lewe dziecko
            }
        }
        delete node;
    }
/*III. wezel ma dwoje dzieci*/else
    {
//wstaw w miejsce usuwanego elementu - najmniejsza wartosc z prawego poddrzewa
        Element *temp;
        temp=min(node->right);
        node->data = temp->data;
        remove(temp);
    }
}

int main() {
    std::cout << "Hello, World!" << std::endl;
    Tree *tree = new Tree;
    tree->insert(8, root);
    tree->insert(7, root);
    tree->insert(9, root);
    tree->in_order_tree_walk(root);
    return 0;
}